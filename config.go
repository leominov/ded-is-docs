package main

import (
	"encoding/json"
	"strings"

	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	LogLevel string `envconfig:"log_level" json:"log_level" default:"info"`

	Confluence struct {
		Password string `envconfig:"password" required:"true" json:"password"`
		URL      string `envconfig:"url" required:"true" json:"url"`
		Username string `envconfig:"username" required:"true" json:"username"`
	} `envconfig:"confluence" json:"confluence"`

	Slack struct {
		APIURL    string `envconfig:"api_url" json:"api_url"`
		Channel   string `envconfig:"channel" json:"channel"`
		Debug     bool   `envconfig:"debug" json:"debug"`
		Enabled   bool   `envconfig:"enabled" json:"enabled"`
		IconEmoji string `envconfig:"icon_emoji" json:"icon_emoji" default:":docusaurus:"`
		IconURL   string `envconfig:"icon_url" json:"icon_url"`
		Username  string `envconfig:"username" json:"username" default:"DED-IS Docs"`
	} `envconfig:"slack" json:"slack"`

	Review   CronJob `envconfig:"review" json:"review"`
	Outdated CronJob `envconfig:"outdated" json:"outdated"`
}

type CronJob struct {
	Enabled   bool   `envconfig:"enabled" json:"enabled"`
	Filter    string `envconfig:"filter" json:"filter"`
	Schedule  string `envconfig:"schedule" json:"schedule"`
	SlackText string `envconfig:"slack_text" json:"slack_text"`
}

func ConfigFromEnv() (*Config, error) {
	c := &Config{}
	err := envconfig.Process("DED_IS", c)
	if err != nil {
		return nil, err
	}
	c.Review.DefineDefaultsForReview()
	c.Outdated.DefineDefaultsForOutdated()
	c.Confluence.URL = strings.TrimRight(c.Confluence.URL, "/")
	return c, nil
}

func (c *CronJob) DefineDefaultsForReview() {
	if len(c.SlackText) == 0 {
		c.SlackText = "Documents for review:"
	}
	if len(c.Filter) == 0 {
		c.Filter = "label='ded-is-review'"
	}
}

func (c *CronJob) DefineDefaultsForOutdated() {
	if len(c.SlackText) == 0 {
		c.SlackText = "Outdated documents:"
	}
	if len(c.Filter) == 0 {
		c.Filter = "label='ded-is-outdated'"
	}
}

func (c *Config) String() string {
	out, _ := json.Marshal(c)
	return string(out)
}
