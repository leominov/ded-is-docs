package main

import (
	"os"
	"os/signal"
	"syscall"

	"github.com/sirupsen/logrus"
)

func main() {
	logrus.SetFormatter(&logrus.JSONFormatter{})

	config, err := ConfigFromEnv()
	if err != nil {
		logrus.Fatal(err)
	}

	if level, err := logrus.ParseLevel(config.LogLevel); err == nil {
		logrus.SetLevel(level)
	}

	logrus.Debug(config.String())

	server, err := NewServer(config)
	if err != nil {
		logrus.Fatal(err)
	}

	logrus.Info("Starting ded-is-docs...")

	go server.Start()

	signCh := make(chan os.Signal, 1)
	signal.Notify(signCh, os.Interrupt, syscall.SIGTERM)
	<-signCh

	logrus.Info("Shutting down")
	server.Stop()
	if err != nil {
		logrus.Fatal(err)
	}

	logrus.Info("Bye")
}
