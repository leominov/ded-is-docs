module github.com/leominov/ded-is-docs

go 1.14

require (
	github.com/dustin/go-humanize v1.0.0
	github.com/essentialkaos/go-confluence v5.0.0+incompatible
	github.com/hashicorp/go-cleanhttp v0.5.1 // indirect
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/robfig/cron/v3 v3.0.0
	github.com/sirupsen/logrus v1.6.0
	github.com/slack-go/slack v0.6.5
	github.com/valyala/fasthttp v1.14.0 // indirect
)
