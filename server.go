package main

import (
	"fmt"
	"strings"

	"github.com/dustin/go-humanize"
	"github.com/essentialkaos/go-confluence"
	"github.com/robfig/cron/v3"
	"github.com/sirupsen/logrus"
	"github.com/slack-go/slack"
)

type searchItem confluence.Content

type Server struct {
	cronCli *cron.Cron
	confCli *confluence.API
	c       *Config
}

func NewServer(c *Config) (*Server, error) {
	cronCli := cron.New(cron.WithSeconds())
	s := &Server{
		cronCli: cronCli,
		c:       c,
	}
	if err := s.setupConfluenceClient(); err != nil {
		return nil, err
	}
	if err := s.setupCronJobs(); err != nil {
		return nil, err
	}
	return s, nil
}

func (s *Server) setupCronJobs() error {
	if s.c.Review.Enabled {
		_, err := s.cronCli.AddFunc(s.c.Review.Schedule, s.runReview)
		if err != nil {
			return err
		}
		logrus.WithField("schedule", s.c.Review.Schedule).
			Info("Review enabled")
	}
	if s.c.Outdated.Enabled {
		_, err := s.cronCli.AddFunc(s.c.Outdated.Schedule, s.runOutdated)
		if err != nil {
			return err
		}
		logrus.WithField("schedule", s.c.Outdated.Schedule).
			Info("Outdated enabled")
	}
	return nil
}

func (s *Server) setupConfluenceClient() error {
	api, err := confluence.NewAPI(s.c.Confluence.URL, s.c.Confluence.Username, s.c.Confluence.Password)
	if err != nil {
		return err
	}
	user, err := api.GetCurrentUser(confluence.ExpandParameters{})
	if err != nil {
		return err
	}
	logrus.Infof("Logged as %s", user.Name)
	s.confCli = api
	return nil
}

func (s *Server) runReview() {
	s.run(s.c.Review)
}

func (s *Server) runOutdated() {
	s.run(s.c.Outdated)
}

func (s *Server) run(c CronJob) {
	var items []searchItem

	logrus.WithField("filter", c.Filter).Info("Requesting documents...")
	opts := confluence.ContentSearchParameters{
		CQL:    c.Filter,
		Expand: []string{"version"},
	}

	collection, err := s.confCli.SearchContent(opts)
	if err != nil {
		logrus.WithField("filter", c.Filter).WithError(err).Error("Failed to get documents")
		return
	}
	logrus.WithField("filter", c.Filter).Debugf("Found %d items", len(collection.Results))

	for _, item := range collection.Results {
		logrus.Debugf("%#v %#v", item.Version.By, item.Version.When)
		items = append(items, searchItem(*item))
	}

	if err = s.notify(c.SlackText, items); err != nil {
		logrus.WithError(err).Error("Failed to notify")
	}
}

func (s *searchItem) AsMarkdown(baseURL string) string {
	if s.Version == nil || s.Version.When == nil || s.Version.By == nil {
		return fmt.Sprintf("<%s|%s>",
			s.WebURL(baseURL),
			s.Title,
		)
	}
	return fmt.Sprintf("<%s|%s>\n%s · By @%s",
		s.WebURL(baseURL),
		s.Title,
		humanize.Time(s.Version.When.Time),
		s.Version.By.Name,
	)
}

func (s *searchItem) WebURL(baseURL string) string {
	if s.Links == nil {
		return ""
	}
	return baseURL + s.Links.WebUI
}

func (s *Server) Start() {
	s.cronCli.Start()
}

func (s *Server) Stop() {
	s.cronCli.Stop()
}

func (s *Server) notify(text string, items []searchItem) error {
	var itemsString []string
	if len(items) == 0 {
		return nil
	}
	for _, item := range items {
		itemMessage := item.AsMarkdown(s.c.Confluence.URL)
		itemsString = append(itemsString, itemMessage)
	}
	attachment := slack.Attachment{
		Text:       strings.Join(itemsString, "\n\n"),
		MarkdownIn: []string{"fallback", "pretext", "text"},
	}
	msg := &slack.WebhookMessage{
		Text:        text,
		IconEmoji:   s.c.Slack.IconEmoji,
		IconURL:     s.c.Slack.IconURL,
		Username:    s.c.Slack.Username,
		Channel:     s.c.Slack.Channel,
		Attachments: []slack.Attachment{attachment},
	}
	if !s.c.Slack.Enabled {
		logrus.Debugf("%#v", msg)
		return nil
	}
	return slack.PostWebhook(s.c.Slack.APIURL, msg)
}
