FROM golang:1.14-alpine3.12 as builder
WORKDIR /go/src/github.com/leominov/ded-is-docs
COPY . .
RUN go build -o ded-is-docs ./

FROM alpine:3.12
COPY --from=builder /go/src/github.com/leominov/ded-is-docs/ded-is-docs /usr/local/bin/ded-is-docs
ENTRYPOINT ["ded-is-docs"]
